/*
 * Created by IntelliJ IDEA
 * User: blankyk
 * Date: 2018/9/13
 * Time: 00:02
 */
package exercise33;

public class Rectangle {
    private double height;
    public Rectangle(double height){
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }
}
