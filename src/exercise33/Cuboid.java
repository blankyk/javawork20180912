/*
 * Created by IntelliJ IDEA
 * User: blankyk
 * Date: 2018/9/12
 * Time: 23:59
 * 定义一个名为Cuboid的长方体类，使其继承Rectangle类其中包括一个表示高的double型成员height；
 * 定义一个构造方法Cuboid（double length，double width，double height）；
 * 再定义一个求长方体体积的volume方法。
 * 编写程序，求一个长宽高分别为10、5、2的长方体体积。
 */
package exercise33;

public class Cuboid extends Rectangle {
    private double length, width;

    public Cuboid(double length, double width, double height) {
        super(height);
        this.length = length;
        this.width = width;
    }

    public double volume() {
        return this.length * this.width * super.getHeight();
    }
}
