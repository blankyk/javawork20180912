/*
 * Created by IntelliJ IDEA
 * User: blankyk
 * Date: 2018/9/12
 * Time: 22:32
 */
package work20180912;

public class TestBus {
    public static void main(String[] args){
        Bus b = new Bus();
        b.start();
        b.speedUP();
        b.stop();
        b.gotOn();
        b.start();
        b.speedUP();
        b.stop();
        b.gotOff();
    }
}
