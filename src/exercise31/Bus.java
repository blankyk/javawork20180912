/*
 * Created by IntelliJ IDEA
 * User: blankyk
 * Date: 2018/9/12
 * Time: 22:25
 */
package work20180912;

public class Bus extends Auto {
    int passenger;

    public void gotOn() {
        passenger += 10;
        System.out.println("bus上客，乘客：" + passenger + "人");
    }

    public void gotOff() {
        passenger += 10;
        System.out.println("bus下客，乘客：" + passenger + "人");
    }
}
