/*
 * Created by IntelliJ IDEA
 * User: blankyk
 * Date: 2018/9/12
 * Time: 22:05
 *
 * 设计一个汽车类Auto，
 * 其中包括一个表达速度的double型的成员变量speed和
 * 表示启动的start方法、
 * 表示加速遗迹停止的stop方法。
 * 再设计一个Auto类的子类Bus表示公共汽车，
 * 在Bus类中定义一个int型的表示乘客数的成员变量passenger，
 * 另外定义两个方法gotOn（）和gotOff（），表示乘客的上车和下车。
 * 编写程序测试Bus类的使用
 */
package work20180912;

public class Auto {
    double speed;//速度

    public void start() {
        this.speed = 0;
        System.out.println("汽车启动，速度为：" + this.speed);
    }

    public void speedUP() {
        this.speed += 10;
        System.out.println("汽车加速，速度为：" + this.speed);
    }

    public void stop() {
        this.speed = 0;
        System.out.println("汽车停止，速度为：" + this.speed);
    }
}
